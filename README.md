# Api cat
## Look for random cats from api  -> https://api.thecatapi.com/v1/favourites

Api cat is api that looks for a random cat pictures, an user can add to their favorites catalog.

## Tech
Dillinger uses a number of open source projects to work properly:

- [GSon] - HTML enhanced for web apps!
- [Okkhtp] - awesome web-based text editor

## Installation

Api cat requires [JDK 11] (https://adoptopenjdk.net/) v8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
cd java-cat-api
java -jar Gatos_app-1.0-SNAPSHOT.jar
```